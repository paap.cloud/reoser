#!/bin/bash -xe

apt update;
apt install -y sshpass rsync;

# https://superuser.com/questions/48783/how-can-i-pass-an-environment-variable-through-an-ssh-command/48787#48787
# https://stackoverflow.com/questions/1636889/how-can-i-configure-it-to-create-target-directory-on-server/22908437#22908437
printf "#!/bin/bash -xe\n\n" > ~/environment.sh;
echo "INITIAL_USER_PASSWORD=${INITIAL_USER_PASSWORD};" >> ~/environment.sh;
echo "KILLER_DEBIAN_RELEASE_NAME=${KILLER_DEBIAN_RELEASE_NAME};" >> ~/environment.sh;
echo "NEW_DEBIAN_RELEASE_NAME=${NEW_DEBIAN_RELEASE_NAME};" >> ~/environment.sh;
rsync --inplace --rsh="sshpass -p ${INITIAL_USER_PASSWORD} ssh -o StrictHostKeyChecking=no -p ${PORT}" ~/environment.sh ${INITIAL_USER_NAME}@host.docker.internal:~/environment.sh;

# ----------------------------------------------------------------------------------------------------------------------
# Создание в памяти исходной системы второй системы "убийцы", к которой можно подключиться по ssh на порт ${KILLER_PORT}
# Заходим в исходную систему под юзером ${INITIAL_USER_NAME} (пароль ${INITIAL_USER_PASSWORD})
# ----------------------------------------------------------------------------------------------------------------------

# https://stackoverflow.com/questions/305035/how-to-use-ssh-to-run-a-local-shell-script-on-a-remote-machine/7363641#7363641
# https://stackoverflow.com/questions/2853803/how-to-echo-shell-commands-as-they-are-executed/2853811#2853811
# https://stackoverflow.com/questions/305035/how-to-use-ssh-to-run-a-local-shell-script-on-a-remote-machine/3872762#3872762
# https://stackoverflow.com/questions/22009364/is-there-a-try-catch-command-in-bash/22010339#22010339
sshpass -p ${INITIAL_USER_PASSWORD} ssh -o StrictHostKeyChecking=no ${INITIAL_USER_NAME}@host.docker.internal -p ${PORT} 'bash -xe' <<'ENDSSH'

  chmod u+x ~/environment.sh;
  . ~/environment.sh;

  # https://www.thegeekdiary.com/how-to-make-alias-command-work-in-bash-script-or-bashrc-file/
  shopt -s expand_aliases;

  # универсальнее смотреть не в файл /etc/os-release, а в uname, т.к. файл /etc/os-release отсутствует в bsd и android системах
  # тем не менее дистрибутив в uname может отсутстовать, поэтому используем оба способа
  if test -f "/etc/os-release"; then
    distribution=$(cat /etc/os-release | grep ^ID= | sed 's/^ID=//');
  else
    distribution=$(uname -a | tr '[:upper:]' '[:lower:]');
  fi

  # https://linuxize.com/post/how-to-check-if-string-contains-substring-in-bash/
  if [[ "$distribution" == *"fedora"* ]]; then
    needUnmounting=true;
    osKernelType=linux;

    alias sudo="echo -n ${INITIAL_USER_PASSWORD} | sudo -S";

    alias sudoSPackageManagerUpdate="sudo dnf upgrade --refresh";
    alias sudoSPackageManagerInstall="sudo dnf install -y dpkg util-linux perl debootstrap";
  elif [[ "$distribution" == *"ubuntu"* ]] || [[ "$distribution" == *"debian"* ]]; then
    needUnmounting=true;
    osKernelType=linux;

    alias sudo="echo -n ${INITIAL_USER_PASSWORD} | sudo -S";

    # https://askubuntu.com/questions/1096930/sudo-apt-update-error-release-file-is-not-yet-valid/1214312#1214312
    alias sudoSPackageManagerUpdate="sudo apt -o Acquire::Check-Date=false update";
    alias sudoSPackageManagerInstall="sudo apt install -y dpkg util-linux perl debootstrap";
  elif [[ "$distribution" == *"opensuse"* ]]; then
    needUnmounting=true;
    osKernelType=linux;

    alias sudo="echo -n ${INITIAL_USER_PASSWORD} | sudo -S";

    alias sudoSPackageManagerUpdate="sudo zypper refresh";
    alias sudoSPackageManagerInstall="sudo zypper install -y dpkg util-linux perl debootstrap";
  elif [[ "$distribution" == *"arch"* ]]; then
    needUnmounting=true;
    osKernelType=linux;

    alias sudo="echo -n ${INITIAL_USER_PASSWORD} | sudo -S";

    alias sudoSPackageManagerUpdate="sudo pacman -Sy";
    alias sudoSPackageManagerInstall="sudo pacman -S --noconfirm dpkg util-linux perl debootstrap";
  elif [[ "$distribution" == *"alpine"* ]]; then
    needUnmounting=true;
    osKernelType=linux;

    alias sudo="echo -n ${INITIAL_USER_PASSWORD} | sudo -S";

    alias sudoSPackageManagerUpdate="sudo apk update";
    alias sudoSPackageManagerInstall="sudo apk add dpkg util-linux perl debootstrap";
  elif [[ "$distribution" == *"android"* ]]; then
    needUnmounting=false;
    osKernelType=android;

    export PATH="$PATH:/system/bin"; # нужно для доступа к lsmod, чтобы искать модули ядра линукс

    alias sudo="sudo";

    # https://askubuntu.com/questions/1096930/sudo-apt-update-error-release-file-is-not-yet-valid/1214312#1214312
    alias sudoSPackageManagerUpdate="apt -o Acquire::Check-Date=false update";
    alias sudoSPackageManagerInstall="apt install -y dpkg util-linux perl debootstrap";

    # нужно в таких системах как андроид, но в других - хуже от этого не станет (тем не менее оставил только для андроида, т.к., например, во freebsd нет опции remount)
    sudo mount -o rw,remount /;
  elif [[ "$distribution" == *"freebsd"* ]]; then
    needUnmounting=false;
    osKernelType=freebsd;

    alias sudo="echo -n ${INITIAL_USER_PASSWORD} | sudo -S";

    alias sudoSPackageManagerUpdate="sudo pkg update";
    alias sudoSPackageManagerInstall="sudo pkg install -y dpkg util-linux perl5 debootstrap";
  else
    echo "Couldn't define linux distribution";
    exit 1;
  fi

  function unlockApt {
    # FIXME только для apt, для других менеджеров пакетов что делать пока неясно (и нужно ли вообще что-то делать)
    # https://pingvinus.ru/note/dpkg-lock
    sudo rm -f /var/lib/apt/lists/lock || true;
    sudo rm -rf /var/cache/apt/archives/ && sudo mkdir -p /var/cache/apt/archives/ || true;
    sudo rm -f /var/lib/dpkg/lock || true;
    sudo rm -f /var/lib/dpkg/lock-frontend || true;
    # https://askubuntu.com/questions/521770/error-parsing-file-var-lib-dpkg-updates-0001-near-line-0-newline-in-field-n/1310633#1310633
    sudo rm -rf /var/lib/dpkg/updates/ && sudo mkdir -p /var/lib/dpkg/updates/ || true;
    sudo dpkg --force-all --configure -a || true;
  }

  function detectArchitecture {
    architecture=$(uname -m);
    # https://github.com/tianon/gosu/issues/24#issuecomment-232625079
    correctedArchitecture=$(echo -n $architecture | awk -F'-' '{print $NF}');

    # в таких системах как termux-андроид программа dpkg работает не так как везде. Результат нужно корректировать
    if [ "$correctedArchitecture" = "x86_64" ] || [ "$correctedArchitecture" = "amd64" ]; then
      correctedArchitecture=amd64;
    elif [ "$correctedArchitecture" = "x86" ] || [ "$correctedArchitecture" = "i386" ] || [ "$correctedArchitecture" = "386" ] || [ "$correctedArchitecture" = "i686" ] || [ "$correctedArchitecture" = "686" ]; then
      correctedArchitecture=i386;
    else
      echo "Couldn't detect architecture for $correctedArchitecture";
      exit 1;
    fi

    echo $correctedArchitecture;
  }

  # Создаем каталог и файловую систему для «Системы убийцы», монтируем её
  sudo mkdir -p /target;
  while [ true ]; do
    # в андроиде иногда сбоит с ошибкой Permission denied
    sudo mount -t tmpfs -o size=1G none /target/ && echo "Mount /target OK" && break || echo "Mount /target ERROR, try one more time";
  done
  sudo mkdir -p /target/root;
  sudo cp ~/environment.sh /target/root/environment.sh;

  # передача переменных в систему "убийцу"
  printf "#!/bin/bash -xe\n\n" > ~/environment_from_origin.sh;
  echo "osKernelType=${osKernelType};" >> ~/environment_from_origin.sh;
  sudo cp ~/environment_from_origin.sh /target/root/environment_from_origin.sh;

  # Ставим отличную утилиту debootstrap, которая разворачивает минимальную установку Debian, при помощи неё мы создадим chroot окружение
  # Существуют аналогичные утилиты для Федоры и Centos, соответственно febootstrap и yumbootstrap
  # https://unix.stackexchange.com/questions/3514/how-to-grep-standard-error-stream-stderr/7080#7080
  errors=$(sudoSPackageManagerUpdate 2>&1 > /dev/null | grep -vi "\bpassword\b" | grep -vi "\bwarning\b:") || true;
  errorsCount=$(echo $errors | wc -w);
  while [ $errorsCount != 0 ]; do
    echo $errors;
    echo "sudoSPackageManagerUpdate ERROR, try one more time";
    unlockApt;
    # https://unix.stackexchange.com/questions/3514/how-to-grep-standard-error-stream-stderr/7080#7080
    errors=$(sudoSPackageManagerUpdate 2>&1 > /dev/null | grep -vi "\bpassword\b" | grep -vi "\bwarning\b:") || true;
    errorsCount=$(echo $errors | wc -w);
  done
  echo "sudoSPackageManagerUpdate OK";

  # в пакет util-linux входит lsblk (нужно, если в каких-то системах этой команды нет) [например, в alpine]
  # perl нужен в некоторых системах для запуска debootstrap [например, в alpine]
  # https://unix.stackexchange.com/questions/3514/how-to-grep-standard-error-stream-stderr/7080#7080
  errors=$(sudoSPackageManagerInstall 2>&1 > /dev/null | grep -vi "\bpassword\b" | grep -vi "\bwarning\b:") || true;
  errorsCount=$(echo $errors | wc -w);
  while [ $errorsCount != 0 ]; do
    echo $errors;
    echo "sudoSPackageManagerInstall ERROR, try one more time";
    unlockApt;
    # https://unix.stackexchange.com/questions/3514/how-to-grep-standard-error-stream-stderr/7080#7080
    errors=$(sudoSPackageManagerInstall 2>&1 > /dev/null | grep -vi "\bpassword\b" | grep -vi "\bwarning\b:") || true;
    errorsCount=$(echo $errors | wc -w);
  done
  echo "sudoSPackageManagerInstall OK";

  # Далее будет отмонтирование разных папок и в некоторых системах может отмонтироваться /home
  # Чтобы избежать ошибки 'shell-init: error retrieving current directory: getcwd: cannot access parent directories: No such file or directory'
  # стоит сменить текущий каталог, который сейчас смотрит в домашний каталог
  cd /;

  # Отмонтируем все, что было примонтировано.
  # Это нужно делать именно здесь, т.к. в системе "убийце" точки монтирования уже не видны. А без этого оторвать диски не получится
  if [ $needUnmounting = true ]; then
    disks=$(lsblk --noheadings | grep disk | grep -vi swap | sed 's/\([a-z]\+\).*/\1/');
    for disk in $disks; do
      # цикл приходится делать для таких файловых систем, которые используются, например,
      # в OpenSuse, где отмонтировал одно, так на это место сразу другое примонтировалось
      while [ true ]; do
        mountPoints=$(lsblk --noheadings /dev/$disk | grep part | grep "/.\+" | sed 's/\([^/]*\)\(\/.*\)/\2/');

        if [ "$mountPoints" = "" ]; then
          break;
        fi

        for mountPoint in $mountPoints; do
          echo "Unmounting point $mountPoint from /dev/$disk"
          # https://stackoverflow.com/questions/7878707/how-to-unmount-a-busy-device/19969471#19969471
          sudo umount -l $mountPoint;
          echo "Point $mountPoint from /dev/$disk unmounted"
        done
      done
    done
  fi

  # В Linux функционал ядра может быть вбилжен в само ядро, либо подключен как модуль
  # И разработчик каждого дистрибутива сам решает, что включать в сборку, а что вынести в модули
  # При этом надо помнить, что система "убийца" еще пользуется ядром исходной системы и своего ядра в chroot пока еще нет
  # Если в системе "убийце" понадобится какой-то функционал, которого в ядре исходной системы нет и этот функционал
  # при старте системы даже не подключен как модуль, то нужно заранее позабится о том, чтобы этот модуль включить
  # https://unix.stackexchange.com/questions/531960/why-doesnt-alias-work-inside-if/531961#531961
  if [[ "$osKernelType" == *"freebsd"* ]]; then
    # https://forums.freebsd.org/threads/install-debian-gnu-linux-using-debootstrap-on-a-freebsd-jail-with-zfs.41470/
    sudo kldload linsysfs;
    # https://forums.freebsd.org/threads/using-sudo-with-a-redirect.39571/#post-219700
    # sudo bash -xec 'echo ''linux_enable="YES"'' >> /etc/rc.conf';
    # sudo sysrc linux_enable="YES";
    # https://www.freebsd.org/cgi/man.cgi?query=lindev&sektion=4&manpath=FreeBSD+8.1-RELEASE
    # https://lists.freebsd.org/pipermail/freebsd-questions/2010-May/216905.html
    # sudo bash -xec 'echo ''lindev_load="YES"'' >> /etc/rc.conf';
    # sudo sysrc lindev_load="YES";
  elif [[ "$osKernelType" == *"linux"* ]]; then
    #https://stackoverflow.com/questions/8901631/how-can-i-get-a-list-of-all-the-active-kernel-drivers-on-my-android-system/14674383#14674383
    ext4ModsCount=$(lsmod | grep ^ext4\\s | wc -l);
    if [ $ext4ModsCount = 0 ]; then
      echo "Switching 'ext4' module on";
      # для таких систем как OpenSuse нужно подключать модуль файловой системы ext4, т.к. opensuse и им подобные системы
      # по умолчанию используют другую файловую систему (например, btrfs как в случае с opensuse)
      # https://www.linuxquestions.org/questions/linux-server-73/not-able-to-mount-ext4-in-sles-11-a-752279/#post4330625
      sudo modprobe ext4 || true; # FIXME true проставлено, потому что в андроиде команда падала. Нужно поправить причину падения и убрать true (см. продолжение ниже)
    fi
    # https://stackoverflow.com/questions/8901631/how-can-i-get-a-list-of-all-the-active-kernel-drivers-on-my-android-system/14674383#14674383
    dmModsCount=$(lsmod | grep ^dm_mod\\s | wc -l);
    if [ $dmModsCount = 0 ]; then
      echo "Switching 'dm_mod' module on";
      # для таких систем как Arch Linux не достает функционала модуля dm_mod
      sudo modprobe dm_mod || true; # FIXME true проставлено, потому что в андроиде команда падала. Нужно поправить причину падения и убрать true (см. продолжение ниже)
    fi
  elif [[ "$osKernelType" == *"android"* ]]; then
    echo "Android. Do nothing.";
  else
    echo "Unknown os kernel type '$osKernelType'";
    exit 1;
  fi

  # Разворачиваем chroot
  # Первый аргумент — версия, второй — каталог установки, третий — репозиторий.
  while [ true ]; do
    # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=637363
    # вместо bullseye для системы "убийцы" лучше использовать buster, т.к. bullseye не везде работает стабильно (например, есть проблемы на андроиде)
    # в таких системах как alpine есть проблемы при скачивании по протоколу https поэтому используем универсальный протокол http (иначе может быть ошибка E: Failed getting release file https://ftp.debian.org/debian/dists/bullseye/Release)
    sudo debootstrap --arch=$(detectArchitecture) ${KILLER_DEBIAN_RELEASE_NAME} /target/ http://ftp.debian.org/debian/ && echo "Debootstrap OK" && break || echo "Debootstrap ERROR, try one more time";
  done

  # Даем имя chroot-окружению
  # Слово «killer» будет показываться в приглашении bash. Это важная штука, без неё будет не понятно, где мы в данный момент находимся.
  # https://askubuntu.com/questions/103643/cannot-echo-hello-x-txt-even-with-sudo
  while [ true ]; do
    # в андроиде иногда сбоит с ошибкой Permission denied
    sudo bash -xec 'echo "killer" > /target/etc/debian_chroot' && echo "Setup killer name OK" && break || echo "Setup killer name ERROR, try one more time";
  done

  # Переходим в новое окружение (будущую систему "убийцу").
  # если не указать шелл, то будет передан тот, что используется в исходной системе, а там
  # может быть какой-нибудь /bin/ash, которого нет в debian. Например, так было в alpine
  #
  # Пока sudo - это алиас на реальный sudo, но с учетом ввода пароля. Однако в случае chroot, если через echo передавать
  # пароль в 'sudo -S', то линукс путается куда именно передавать значение из echo: в 'sudo -S' или в '/bin/bash', поэтому
  # предварительно нужно алиас разалиасить, а так как пароль уже много раз вводился выше, то sudo пароль не затребует.
  unalias sudo;

  # для андроида '/bin/bash' пришлось заменить на 'bin/bash'. В остальных системах работает и так и так
  sudo chroot /target bin/bash -xe;

  # нужно только в андроиде и только для обеспечения чистоты лога. В остальных системах не мешает работе
  # https://github.com/termux/termux-app/issues/1576#issuecomment-633252682
  unset LD_PRELOAD;

  # Вызываемые в системе "убийце" программы лежат именно в chroot-е, но переменная PATH используется из исходной системы
  # и поэтому может содержать неадекватные для Debian пути. По этой причине переменную PATH нужно заменить путями для
  # Debian (с данной проблемой можно столкнуться, например, в Arch https://bbs.archlinux.org/viewtopic.php?id=196922)
  export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin";

  # Андроид очень сопротивляется созданию и настройке групп и пользователей даже в chroot-окружении
  # Поэтому такие вещи будем делать через правку файликов, а не через вызов таких команд как adduser, groupadd и usermod
  # Несмотря на то, что эти команды нужны только для андроида, в других системах они помешать не должны
    # получаем следующий свободный uuid (https://www.kevinguay.com/posts/next-uid-gid/)
    function nextUuid {
      echo $(cat /etc/group /etc/passwd | cut -d ":" -f 3 | grep "^3...$" | sort -n | tail -n 1 | awk '{ print $1+1 }');
    }

    # https://stackoverflow.com/questions/36451444/what-can-cause-a-socket-permission-denied-error/36451445#36451445
    # Создаем необходимые группы с полным набором прав в андроиде
    # Имена групп не имеют значения, главное GID
    # Это необходимо делать только в андроиде, но в других системах хуже от этих групп не станет
    echo "android_bt:x:3001:root,_apt,sshd" >> /etc/group; # скорее всего используется в андроид, для того чтобы разрешить доступ к bluetooth
    echo "android_bt_net:x:3002:root,_apt,sshd" >> /etc/group; # скорее всего используется в андроид, для того чтобы разрешить доступ к bluetooth
    # https://askubuntu.com/questions/910865/apt-get-update-fails-on-chroot-ubuntu-16-04-on-android/940851#940851
    echo "android_inet:x:3003:root,_apt,sshd" >> /etc/group; # нужно, чтобы ping работал (открывался сокет)
    echo "android_net_raw:x:3004:root,_apt,sshd" >> /etc/group; # нужно, чтобы ping работал (открывался сокет)
    echo "android_admin:x:3005:root,_apt,sshd" >> /etc/group;

    # Заводим пользователя для менеджера пакетов apt: UID выбираем следующий свободный, а группу даем 3003, для которой в андроиде разрешен доступ в интернет
    # https://askubuntu.com/questions/882039/no-sandbox-user-apt-on-the-system-can-not-drop-privileges/882086#882086
    # https://stackoverflow.com/questions/43724042/apt-get-update-fails-on-chroot-ubuntu-16-04-on-android/45188246#45188246
    # https://askubuntu.com/questions/910865/apt-get-update-fails-on-chroot-ubuntu-16-04-on-android/940851#940851
    echo "_apt:x:$(nextUuid):3003::/nonexistent:/bin/false" >> /etc/passwd;
    echo "_apt:*:17121:0:99999:7:::" >> /etc/shadow;

    # Заводим пользователя для ssh-сервера: UID выбираем следующий свободный, а группу даем 3003, для которой в андроиде разрешен доступ в интернет
    # http://www.nclug.ru/forum/shhd-i-privilege-separation-user-sshd-does-not-exist
    echo "sshd:x:$(nextUuid):3003::/nonexistent:/bin/false" >> /etc/passwd;
    echo "sshd:*:17121:0:99999:7:::" >> /etc/shadow;

    su; # почему-то без этого в андроиде мы будем находиться в chroot-окружении без подписи killer

  chmod u+x ~/environment_from_origin.sh;
  . ~/environment_from_origin.sh;

  # Монтируем полезные fs
  # Без этих файловых систем ничего сделать не получится, т.е. нужна поддержка этих файловых систем со стороны ядра исходной системы
  # Во freebsd с этим проблемы (sysfs поддерживается не полностью, а devtmpfs и devpts не поддерживаются вообще). В андроиде такая же беда - devtmpfs и devpts не поддерживаются
  # https://superuser.com/questions/165116/mount-dev-proc-sys-in-a-chroot-environment
  mount none -t proc /proc/;
  if [[ "$osKernelType" == *"freebsd"* ]]; then
    mount none -t linsysfs /sys/;
  elif [[ "$osKernelType" == *"linux"* ]] || [[ "$osKernelType" == *"android"* ]]; then
    mount none -t sysfs /sys/;
  else
    echo "Unknown os kernel type '$osKernelType'";
    exit 1;
  fi
  mount none -t devtmpfs /dev/ || true; # FIXME вызывает ошибку в андроиде и freebsd "mount: /dev: unknown filesystem type 'devtmpfs'". После разрешения нужно убрать true
  mount none -t devpts /dev/pts/ || true; # FIXME вызывает ошибку в андроиде и freebsd "mount: /dev/pts/: mount point does not exist", из-за того что pts вложено в dev, в dev выше не примонтировалось. После разрешения нужно убрать true

  # Дальше мои заморочки: у дебиановского пакета openssh-server в рекомендованных пакетах есть пакет xauth,
  # а у него в зависимостях всякие иксовые библиотеки. Я, как сторонник минимализма, не хочу, чтобы на сервере,
  # где не было и не будет графики, ставились огрызки иксов. Поэтому ставим с ключиком --no-install-recommends
  apt update;
  while [ true ]; do
    # в андроиде есть проблемы с конфигурированием скачанных пакетов при помощи dpkg, поэтому помогаем (в других системах хуже не будет)
    apt install -y openssh-server openssh-client at --no-install-recommends && echo "Ssh and utils install OK" && break ||
    echo "Ssh and utils install ERROR, try one more time" && rm -rf /var/lib/dpkg/info && mkdir -p /var/lib/dpkg/info;
  done

  # запуск atd сервиса, через который работает команда at
  /etc/init.d/atd restart; # не знаю почему, но нужно вызвать до пересоздания /dev/null
  /etc/init.d/atd status;

  # Создаем ssh-конфиги. Ставим альтернативный порт для ssh демона, чтобы мы могли зайти на chroot систему по ssh.
  # И разрешаем доступ для root. Можно не давать доступ root, а создать пользователя и дать ему sudo права, но тут я сознательно упрощаю.
  echo "Port 11122" > /etc/ssh/sshd_config;
  echo "PermitRootLogin yes" >> /etc/ssh/sshd_config;

  # В андроиде без этого будут warning-и, в других же системах хуже не станет
  # https://bbs.archlinux.org/viewtopic.php?id=165382
  ssh-keygen -A;

  # в андроиде есть проблемы с запуском ssh сервера из-за проблем с null-устройством, поэтому его нужно пересоздать (в других системах хуже не станет)
  # The solution is to rebuild a character device after deleting /dev/null: https://titanwolf.org/Network/Articles/Article?AID=9f3d95f9-278f-484e-8fb7-0f99a0d202b7
  rm -f /dev/null;
  mknod /dev/null c 1 3; # https://ru.wikipedia.org/wiki//dev/null

  /etc/init.d/ssh restart;

  # Дальше надо задать пароль root, так как по умолчанию debootstrap не устанавливает никакие пароли
  # https://askubuntu.com/questions/80444/how-to-set-user-passwords-using-passwd-without-a-prompt/1197185#1197185
  echo root:2 | chpasswd;

ENDSSH

ssh-keygen -R [host.docker.internal]:${KILLER_PORT};

# ----------------------------------------------------------------------------------------------------------------------
# Заходим в систему "убийцу" под root (пароль 2)
# ----------------------------------------------------------------------------------------------------------------------

# Заходим в chroot окружение по ssh
# Это мы делаем для того, чтобы полностью отвязаться от старой системы, у которой мы оторвем диски.
# А так у нас будет полностью автономная система в оперативной памяти, никак не связанная со старой.
sshpass -p 2 ssh -o StrictHostKeyChecking=no root@host.docker.internal -p ${KILLER_PORT} 'bash -xe' <<'ENDSSH'

  chmod u+x ~/environment.sh;
  . ~/environment.sh;

  function getDisks {
    echo $(lsblk --output NAME,TYPE,MOUNTPOINT --noheadings | grep disk | grep -vi swap | sed 's/disk//g');
  }

  function detectArchitecture {
    architecture=$(dpkg --print-architecture);
    # https://github.com/tianon/gosu/issues/24#issuecomment-232625079
    correctedArchitecture=$(echo -n $architecture | awk -F'-' '{print $NF}');
    echo $correctedArchitecture;
  }

  # Еще раз ставим debootstrap и доп.утилиты
  while [ true ]; do
    # в андроиде есть проблемы с конфигурированием скачанных пакетов при помощи dpkg, поэтому помогаем (в других системах хуже не будет)
    apt install -y lvm2 debootstrap parted arch-install-scripts && echo "Install debootstrap and utils OK" && break ||
    echo "Install debootstrap and utils ERROR, try one more time" && rm -rf /var/lib/dpkg/info && mkdir -p /var/lib/dpkg/info;
  done

  # Находим все диски
  disks=$(getDisks);
  disksCountBegin=$(echo $disks | wc -w);

  # Затираем диски, чтобы они ни в коем случае не подцепились LVM-ом
  for disk in $disks; do
    echo "Cleaning $disk";

    # в андроиде zero устройство нужно создать (в остальных системах его пересоздание не повредит)
    rm -f /dev/zero;
    mknod /dev/zero c 1 5; # https://ru.wikipedia.org/wiki//dev/zero

    dd if=/dev/zero of=/dev/${disk} bs=1M count=100;
  done

  # Отрываем диски
  for disk in $disks; do
    echo "Breaking out $disk";
    # https://habr.com/ru/post/102387/
    echo 1 > /sys/block/${disk}/device/delete;
  done

  # Проверяем, что диски оторвались
  disks=$(getDisks);
  disksCount=$(echo $disks | wc -w);
  if [ $disksCount = 0 ]; then
    echo "Disks broke out";
  else
    echo "Disks not broke out";
    exit 1;
  fi

  # Подключаем диски обратно
  for scsiHost in /sys/class/scsi_host/host?; do
    ls -al $scsiHost; # FIXME нужно удалить, оставлено на время отладки
    echo "- - -" > ${scsiHost}/scan;
  done

  # Проверяем, что диски вернулись
  disks=$(getDisks);
  disksCountEnd=$(echo $disks | wc -w);
  while [ $disksCountBegin != $disksCountEnd ]; do
    echo "Disks not returned, try wait...";
    sleep 10;

    disks=$(getDisks);
    disksCountEnd=$(echo $disks | wc -w);
  done
  echo "Disks returned";

  # На загрузочном диске необходимо создать один первичный раздел размером на весь диск, и этот раздел отдать LVM-у, для того чтобы на него смог встать grub.
  # Все остальные диски можно отдавать LVM-у целиком, не создавая систему разделов (pvcreate /dev/sdc).
  #
  # Создаем таблицу разделов и один первичный раздел типа 8e (Linux LVM) на весь диск:
  counter=0;
  for disk in $disks; do
    # Удаление разделов
    echo "Removing partitions";
    wipefs -a /dev/${disk}; # FIXME в андроиде пока застрял с ошибкой "wipefs: error: /dev/sdb: probing initialization failed: No such file or directory"
    # https://www.cyberciti.biz/tips/re-read-the-partition-table-without-rebooting-linux-system.html
    partprobe /dev/${disk};

    # Создание новых разделов
    echo "Creating partitions";
    if [ $counter = 0 ]; then
      # нужен отдельный микрораздел под grub
      parted -a optimal /dev/${disk} --script mklabel gpt unit MiB mkpart primary ext2 0% 2MiB mkpart primary ext4 2MiB 100%;
    else
      parted -a optimal /dev/${disk} --script mklabel gpt mkpart primary ext4 0 100%;
    fi
    # https://www.cyberciti.biz/tips/re-read-the-partition-table-without-rebooting-linux-system.html
    partprobe /dev/${disk};

    # Задание типов разделов
    echo "Setting types for partitions";
    if [ $counter = 0 ]; then
      # http://tmel.ru/vosstanovlenie-zagruzchika-grub-2-v-linux-ubuntu/
      # https://www.gnu.org/software/parted/manual/html_node/set.html
      parted /dev/${disk} set 1 bios_grub on; # нужен отдельный микрораздел под grub
      parted /dev/${disk} set 2 lvm on;
    else
      parted /dev/${disk} set 1 lvm on;
    fi
    # https://www.cyberciti.biz/tips/re-read-the-partition-table-without-rebooting-linux-system.html
    partprobe /dev/${disk};

    counter=$((counter +1));
  done

  # Проверяем, что сделали
  disks=$(getDisks);
  counter=0;
  for disk in $disks; do
    partsCount=$(fdisk -l | grep "/dev/${disk}[0-9]\+" | wc -l);
    echo "Disk $disk has $partsCount parts";

    if [ $counter = 0 ]; then
      if [ $partsCount != 2 ]; then
        exit 1;
      fi
    else
      if [ $partsCount != 1 ]; then
        exit 1;
      fi
    fi

    counter=$((counter +1));
  done

  # Создание LVM в 1-ом разделе 1-ого диска
  # Внимание надо обратить на порядок создания точек монтирования и собственно монтирования разделов.
  #
  counter=0;
  for disk in $disks; do
    if [ $counter = 0 ]; then
      # создание физического вольюма
      pvcreate --force /dev/${disk}2;

      # создание вольюм-группы vg_root
      vgcreate vg_root /dev/${disk}2;

      # создание логического вольюма lv_swap0
      lvcreate -Zn -L500M -n lv_swap0 vg_root;

      # создание логического вольюма lv_root
      lvcreate -Zn -L1G -n lv_root vg_root;
      # создание логического вольюма lv_usr
      lvcreate -Zn -L2G -n lv_usr vg_root;
      # создание логического вольюма lv_var
      lvcreate -Zn -L2G -n lv_var vg_root;
      # создание логического вольюма lv_var_log
      lvcreate -Zn -L1G -n lv_var_log vg_root;
      # создание логического вольюма lv_home
      lvcreate -Zn -L1G -n lv_home vg_root;
    else
      # создание физического вольюма
      pvcreate --force /dev/${disk}1;
    fi

    counter=$((counter +1));
  done

  # https://serverfault.com/questions/827251/cannot-do-lvcreate-not-found-device-not-cleared-on-centos/1059400#1059400
  vgmknodes;

  # форматирование созданных логических вольюмов
  mkswap /dev/vg_root/lv_swap0;
  mkfs.ext4 /dev/mapper/vg_root-lv_root;
  mkfs.ext4 /dev/mapper/vg_root-lv_usr;
  mkfs.ext4 /dev/mapper/vg_root-lv_var;
  mkfs.ext4 /dev/mapper/vg_root-lv_var_log;
  mkfs.ext4 /dev/mapper/vg_root-lv_home;

  # монтируем LVM
  mkdir /target;
  mount /dev/mapper/vg_root-lv_root /target/;
  mkdir /target/usr;
  mount /dev/mapper/vg_root-lv_usr /target/usr;
  mkdir /target/var;
  mount /dev/mapper/vg_root-lv_var /target/var;
  mkdir /target/var/log;
  mount /dev/mapper/vg_root-lv_var_log /target/var/log;
  mkdir /target/home;
  mount /dev/mapper/vg_root-lv_home /target/home;

  # Разворачиваем уже боевую new систему на новое место на жестком диске
  while [ true ]; do
    # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=637363
    debootstrap --arch=$(detectArchitecture) ${NEW_DEBIAN_RELEASE_NAME} /target/ https://ftp.debian.org/debian/ && echo "Debootstrap OK" && break || echo "Debootstrap ERROR, try one more time";
  done

  echo "new" > /target/etc/debian_chroot;

  # Создаем дисковые конфиги при помощи утилиты от arch linux из пакета arch-install-scripts, который также портирован в debian
  genfstab -U /target >> /target/etc/fstab;

  # Теперь нас ждет новая система
  chroot /target /bin/bash -xe;

  # Создаем сетевые конфиги (без них не получится попасть в переустановленную систему)
  # Причем на данном этапе нам не известны реальные имена сетевых устройств в новой системе
  # в этой точке мы все еще видим имена сетевых устройств исходной системы
  # поэтому просто готовим скрипт и сервис, который отработает при перезагрузке системы и настроит сеть
    # https://ostechnix.com/fix-exec-format-error-when-running-scripts-with-run-parts-command/
    echo "#!/bin/bash" > /usr/bin/network-prepare.sh;
    echo "" >> /usr/bin/network-prepare.sh;
    echo "networkDevices=\$(ls -al /sys/class/net/ | grep ^l | sed 's/\(.*\)\/\(.*\)/\2/' | grep \"^\(eth\|enp\|wlan\)\");" >> /usr/bin/network-prepare.sh;
    echo "networkDevicesCount=\$(echo \${networkDevices} | wc -w);" >> /usr/bin/network-prepare.sh;
    echo "if [ \$networkDevicesCount != 1 ]; then" >> /usr/bin/network-prepare.sh;
    echo "  echo \"Network devices count is $networkDevicesCount but must be 1\";" >> /usr/bin/network-prepare.sh;
    echo "  exit 1;" >> /usr/bin/network-prepare.sh;
    echo "fi" >> /usr/bin/network-prepare.sh;
    echo "" >> /usr/bin/network-prepare.sh;
    echo "echo \"auto \${networkDevices}\" > /etc/network/interfaces;" >> /usr/bin/network-prepare.sh;
    echo "echo \"iface \${networkDevices} inet dhcp\" >> /etc/network/interfaces;" >> /usr/bin/network-prepare.sh;
    echo "" >> /usr/bin/network-prepare.sh;
    echo "/etc/init.d/networking restart;" >> /usr/bin/network-prepare.sh;
    chmod +x /usr/bin/network-prepare.sh;

    # https://tecadmin.net/run-shell-script-as-systemd-service/
    echo "[Unit]" > /etc/systemd/system/network-prepare.service;
    echo "Description=/usr/bin/network-prepare.sh" >> /etc/systemd/system/network-prepare.service;
    echo "" >> /etc/systemd/system/network-prepare.service;
    echo "[Service]" >> /etc/systemd/system/network-prepare.service;
    echo "ExecStart=/usr/bin/network-prepare.sh" >> /etc/systemd/system/network-prepare.service;
    echo "" >> /etc/systemd/system/network-prepare.service;
    echo "[Install]" >> /etc/systemd/system/network-prepare.service;
    echo "WantedBy=multi-user.target" >> /etc/systemd/system/network-prepare.service;

    # https://unix.stackexchange.com/questions/369017/enable-systemd-services-without-systemctl
    # альтернатива вызову 'systemctl enable network-prepare.service', который в незапущенной системе невозможен
    ln -s /etc/systemd/system/network-prepare.service /etc/systemd/system/multi-user.target.wants/network-prepare.service;

  # после chroot определения функций теряются, поэтому определяем заново
  function getDisks {
    echo $(lsblk --output NAME,TYPE,MOUNTPOINT --noheadings | grep disk | grep -vi swap | sed 's/disk//g');
  }
  function detectImageArchitecture {
    architecture=$(dpkg --print-architecture);
    # https://github.com/tianon/gosu/issues/24#issuecomment-232625079
    correctedArchitecture=$(echo -n $architecture | awk -F'-' '{print $NF}');
    if [ "$correctedArchitecture" = "amd64" ]; then
      imageArchitecture=amd64;
    elif [ "$correctedArchitecture" = "i686" ] || [ "$correctedArchitecture" = "i386" ]; then
      imageArchitecture=686;
    else
      echo "Couldn't define linux image architecture for '$correctedArchitecture'";
      exit 1;
    fi
    echo $imageArchitecture;
  }

  # Монтируем файловые системы
  mount none -t proc /proc/;
  mount none -t sysfs /sys/;
  mount none -t devtmpfs /dev/;
  mount none -t devpts /dev/pts/;

  # Устанавливаем и конфигурируем openssh
  while [ true ]; do
    # в случае подвисания интернета нужно делать повторные попытки
    apt update && apt install -y openssh-server openssh-client --no-install-recommends && echo "Install ssh OK" && break || echo "Install ssh ERROR, try one more time";
  done

  echo "Port 22" > /etc/ssh/sshd_config;
  echo "PermitRootLogin yes" >> /etc/ssh/sshd_config;

  echo root:3 | chpasswd;

  # Устанавливаем пакеты, без которых не обойтись
  #
  # https://superuser.com/questions/1268589/unable-to-locate-package-linux-image-4-8-0-53-generic/1269233#1269233
  # apt-cache search linux-image | grep linux-image-3.16
  #
  # echo "deb https://ftp.debian.org/debian/ bullseye main" > /etc/apt/sources.list
  # apt update
  #
  apt install -y nano sudo linux-image-$(detectImageArchitecture) lvm2 psmisc vlan grub-pc-bin grub2-common dnsutils;

  # Настраиваем grub
  # https://askubuntu.com/questions/531364/non-interactive-install-of-grub2-when-installing-from-scratch/1107229#1107229
  mainDisk=$(getDisks | head -n 1);
  grub-install /dev/${mainDisk};
  update-grub;

  # Меняем hostname
  echo new-system > /etc/hostname;

  # Добавляем строчку в /etc/hosts
  echo "127.0.0.1 new-system" >> /etc/hosts;

  # Добавляем админа
  # https://askubuntu.com/questions/94060/run-adduser-non-interactively/94067#94067
  adduser --disabled-login --gecos "" admin;
  echo admin:3 | chpasswd;
  usermod -a -G sudo admin;

  # Размонтируем файловые системы
  umount /dev/pts;
  umount /dev/;
  umount /proc/;
  umount /sys/;

  # И выходим из chroot'а
  exit;

  # Мы снова в системе "убийце"
  umount /target/usr/;
  umount /target/var/log/;
  umount /target/var/;
  umount /target/home/;
  umount /target/;

  # После этого можно перегружаться.
  # Активируем SysRq (он, скорее всего, активирован, но нам же надо убедиться?).
  echo 1 > /proc/sys/kernel/sysrq;
  # И перезагружаемся, отстрелив фоновый процесс, чтобы потом успеть выйти из ssh-соединения
  echo "sleep 10; echo b > /proc/sysrq-trigger;" | at now;
  exit;

ENDSSH

function updateKnownHosts {
  # Удаляем из файла ~/.ssh/known_hosts старую запись, чтобы ее перезаписать на аналогичную (не совсем понимаю зачем это нужно, но нужно)
  ssh-keygen -R [host.docker.internal]:${PORT};
}

updateKnownHosts;

# Ждём рестарта новой системы и заодно проверяем, что вход пользователем admin разрешен
until sshpass -p 3 ssh -o StrictHostKeyChecking=no admin@host.docker.internal -p ${PORT}; do
  echo "Trying connect to host.docker.internal by admin";
  sleep 5;
  updateKnownHosts;
done

updateKnownHosts;

# Заходим в новую систему root-ом, т.к. иначе не получится проверить работу DNS
sshpass -p 3 ssh -o StrictHostKeyChecking=no root@host.docker.internal -p ${PORT} 'bash -xe' <<'ENDSSH'

  # Проверяем, что диски примонтированы
  # проверяем наличие swap
  lvSwap=$(lsblk | grep vg_root-lv_swap0);
  if [ "$lvSwap" != "" ]; then
    echo "SWAP mounted";
  else
    echo "SWAP not mounted";
    exit 1;
  fi
  # проверяем наличие root
  lvRoot=$(lsblk | grep vg_root-lv_root);
  if [ "$lvRoot" != "" ]; then
    echo "/ mounted";
  else
    echo "/ not mounted";
    exit 1;
  fi
  # проверяем наличие usr
  lvUsr=$(lsblk | grep vg_root-lv_usr);
  if [ "$lvUsr" != "" ]; then
    echo "/usr mounted";
  else
    echo "/usr not mounted";
    exit 1;
  fi
  # проверяем наличие var
  lvVar=$(lsblk | grep vg_root-lv_var);
  if [ "$lvVar" != "" ]; then
    echo "/var mounted";
  else
    echo "/var not mounted";
    exit 1;
  fi
  # проверяем наличие var_log
  lvVarLog=$(lsblk | grep vg_root-lv_var_log);
  if [ "$lvVarLog" != "" ]; then
    echo "/var/log mounted";
  else
    echo "/var/log not mounted";
    exit 1;
  fi
  # проверяем наличие home
  lvHome=$(lsblk | grep vg_root-lv_home);
  if [ "$lvHome" != "" ]; then
    echo "/home mounted";
  else
    echo "/home not mounted";
    exit 1;
  fi

  # В Debian проверить статус сетевых служб можно командой systemctl status networking
  # Проверяем, что DNS работает
  googleCount=$(sudo nslookup google.com | grep "Name:" | wc -l);
  if [ $googleCount != 0 ]; then
    echo "DNS working";
  else
    echo "DNS not working";
    exit 1;
  fi

  exit;

ENDSSH

# Позволяем контейнеру не останавливаться
tail -f /dev/null;