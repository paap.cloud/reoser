# Команды в Alpine, выполненные через консоль, чтобы установить систему и можно было работать с виртуальной машиной по ssh

Установку системы производим на основе статьи https://losst.ru/ustanovka-alpine-linux
(с момента написания статьи почти ничего не изменилось).
Нужно выполнить с 4 по 14 шаги включительно, выполняя установку openssh в том числе.

После установки заходим в систему под root-ом с паролем, который задали во время установки.
Добавление нового пользователя в систему
```
apk add sudo;
sed -i 's/^# %wheel ALL=(ALL) ALL$/%wheel ALL=(ALL) ALL/' /etc/sudoers;

sed -i 's/^# http:/http:/' /etc/apk/repositories; # раскомментировать репозиторий сообщества
apk update;
apk add shadow; # чтобы добавить в систему такие команды как useradd и chpasswd
apk add bash;

useradd -m -g users -G wheel -s /bin/bash peaceful-coder;
echo peaceful-coder:1 | chpasswd;
```

После этого нужно через ssh войти в систему новым пользователм и закончить установку.
```
ssh-keygen -R [localhost]:10722;
ssh -o StrictHostKeyChecking=no peaceful-coder@localhost -p 10722;

sudo apk add rsync;
```