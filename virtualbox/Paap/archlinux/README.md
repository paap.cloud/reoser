# Команды в Arch, выполненные через консоль, чтобы установить систему и можно было работать с виртуальной машиной по ssh

Установка системы на основе статьи https://losst.ru/ustanovka-archlinux-2016 с небольшими улучшениями

Сначала надо войти в инсталлятор и выполнить там
```
echo root:1 | chpasswd;
```
чтобы задать известный мне пароль root-а

После этого можно через ssh выполнить установку archlinux в виртуалке. Далее работаем в git bash (на Windows)
```
ssh-keygen -R [localhost]:10522;
ssh -o StrictHostKeyChecking=no root@localhost -p 10522;
```

Установка системы и ssh сервера
```
parted -a optimal /dev/sda --script mklabel gpt unit MiB mkpart primary ext2 0% 300MiB mkpart primary ext2 300MiB 301Mib mkpart primary ext2 301MiB 4096Mib mkpart primary ext4 4096MiB 100%;
parted /dev/sda set 2 bios_grub on;

echo y | mkfs -t ext2 -L Boot /dev/sda1;
echo y | mkfs -t ext4 -L Root /dev/sda4;
mkswap /dev/sda3;

mount /dev/sda4 /mnt;
mkdir /mnt/boot;
mount /dev/sda1 /mnt/boot;
swapon /dev/sda3;

pacstrap /mnt base linux linux-firmware;

genfstab -U /mnt >> /mnt/etc/fstab;

arch-chroot /mnt;

ln -s /usr/share/zoneinfo/Europe/Moscow /etc/localtime;
hwclock --systohc --utc;

sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen;
sed -i 's/#ru_RU.UTF-8/ru_RU.UTF-8/' /etc/locale.gen; 
locale-gen;
echo "LANG=ru_RU.UTF-8" > /etc/locale.conf;

echo "FONT=UniCyr_8x16" > /etc/vconsole.conf;

echo "arch-pc" > /etc/hostname;
echo "127.0.0.1 arch-pc.localdomainarch-pc" >> /etc/hosts;

mkinitcpio -plinux;

echo root:1 | chpasswd;

pacman -S --noconfirm sudo;
sed -i 's/^# %wheel ALL=(ALL) ALL$/%wheel ALL=(ALL) ALL/' /etc/sudoers;

useradd -m -g users -G wheel -s /bin/bash peaceful-coder;
echo peaceful-coder:1 | chpasswd;

pacman -S --noconfirm grub;
grub-install /dev/sda;
grub-mkconfig -o /boot/grub/grub.cfg;

pacman -S --noconfirm dhcpcd;
printf "\ny\n" | pacman -S netctl;
ethInterface=$(ls -al /sys/class/net/ | grep ^l | grep -v "lo ->" | sed 's/\(.*\)\/\(.*\)/\2/');
cat /etc/netctl/examples/ethernet-dhcp | sed "s/Interface=eth0/Interface=${ethInterface}/" > /etc/netctl/ethernet-dhcp;
netctl enable ethernet-dhcp;

pacman -S --noconfirm openssh;
sed -i 's/^#\?Port .*$/Port 22/' /etc/ssh/sshd_config;
sed -i 's/^#\?PermitRootLogin .*$/PermitRootLogin yes/' /etc/ssh/sshd_config;
systemctl enable sshd;
pacman -S --noconfirm rsync;

exit;

umount -R /mnt;

reboot;
```
