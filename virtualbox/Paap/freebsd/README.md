# Действия в FreeBSD, чтобы можно было работать с виртуальной машиной по ssh

Важно!
При инсталляции выбрать файловую системы UFS, а не ZFS, которая по умолчанию (в противном случае установка системы зависает)
Убедиться, что при инсталляции выбрана автозагрузка sshd
Создать пользователя peaceful-coder с паролем 1
Задать пользователю root пароль 1
Пройти перед закрытием инсталлятора в консоль, чтобы разблокировать созданного пользователя командой
```shell
pw unlock peaceful-coder
```

После перезагрузки нужно будет войти в систему под root и разрешить ему ходить в систему по ssh
```shell
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
service sshd restart
```

Далее можно будет подключиться по ssh из под root и выполнить оставшееся конфигурирование
```shell
pkg update -y
pkg upgrade

pkg install -y sudo
#https://stackoverflow.com/questions/35620009/sed-extra-characters-at-end-of-l-command/35620381#35620381
sed -i .bak 's/^# %wheel ALL=(ALL) ALL$/%wheel ALL=(ALL) ALL/' /usr/local/etc/sudoers;
pw group mod wheel -m peaceful-coder;
exit;
```

Далее можно будет подключиться по ssh из под peaceful-coder и выполнить оставшееся конфигурирование
```shell
echo -n 1 | sudo -S pkg install -y rsync
echo -n 1 | sudo -S pkg install -y bash
```