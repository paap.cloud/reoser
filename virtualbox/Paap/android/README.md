# Действия в Android x86, чтобы можно было работать с виртуальной машиной по ssh

1. В настройках виртуальной машины:
    - поставить 64 Мб видеопамяти и выставить VBoxVGA (иначе не получится увидеть графический интерфейс андроида)
    - систему выбрать Other 64 (иначе VirtWifi не будет виден)
    - выставить 4 ядра (без этого возникает kernel panic)
1. При установке нужно подключиться к google аккаунту, для того чтобы через Google Play в последствии установить Termux
1. Установить Termux через Google Play
1. Далее работа ведется в терминале Termux

   Обновление системы
   ```
   # Заменяем битые репозитории
   #echo "deb https://mirrors.bfsu.edu.cn/termux/science-packages-24/ science stable" > $PREFIX/etc/apt/sources.list.d/science.list;
   #echo "deb https://mirrors.bfsu.edu.cn/termux/game-packages-24/ games stable" > $PREFIX/etc/apt/sources.list.d/game.list;
   echo "deb https://mirrors.bfsu.edu.cn/termux/termux-packages-24/ stable main" > $PREFIX/etc/apt/sources.list;
   
   apt -o Acquire::Check-Date=false update -y; # когда команда спросит заменить ли ей только что выставленные зеркала в sources.list на официальный репозиторий termux, нужно ответить отказом "N"
   apt upgrade -y;
   ```
   
   Установка ssh сервера
   ```
   apt install -y openssh rsync tsu;
   
   printf "1\n1" | passwd;
   sshd;
   ```

1. Далее работа ведется в git bash

   Вход по ssh из git bash
   ```
   ssh-keygen -R [localhost]:11222;
   ssh -o StrictHostKeyChecking=no any@localhost -p 11222; # пользователь не имеет значения, после входа я буду обычным пользователем андроида
   ```
   
   После входа по ssh выполняем донастройку андроида
   ```
   su; # вылезет окно, которое запросит подтвердить выдачу root прав. Выбрать "Разрешить навсегда"
   ```

# Полезные ссылки

1. Установка ssh в termux https://habr.com/ru/sandbox/141668/
1. Debootstrap в Android https://www.opennet.ru/tips/3194_android_debian_root_xfce_chroot_debootstrap.shtml
1. Сделать / readable-writable https://stackoverflow.com/questions/6066030/read-only-file-system-on-android/8534641#8534641
1. Решение проблемы при работе через ssh с Android https://stackoverflow.com/questions/7114990/pseudo-terminal-will-not-be-allocated-because-stdin-is-not-a-terminal/7122115#7122115
1. Решение проблемы с пропавшим pkg после перехода из termux в su https://github.com/termux/termux-app/issues/211#issuecomment-268388365

# Предупреждение termux на будущее. Стоит проработать этот момент

```
You are likely using a very old version of Termux,
probably installed from the Google Play Store.
There are plans in the near future to remove the
Termux apps from the Play Store so that new users
cannot install them and to **disable** them for
existing users with app updates to prevent the use
of outdated app versions. Instead, you are
encouraged to move to F-Droid or Github sources
(see [1]). You can backup all your current Termux
data before uninstallation and then restore it later
by following instructions in the wiki [2]. Check
the changelog [3] for all the new features and fixes
that you are currently missing. Check [4] for why
this is being done.

[1] https://github.com/termux/termux-app#installation
[2] https://wiki.termux.com/wiki/Backing_up_Termux
[3] https://github.com/termux/termux-app/releases
[4] https://github.com/termux/termux-app#google-play-store-deprecated
```