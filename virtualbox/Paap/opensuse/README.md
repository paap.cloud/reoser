# Команды в OpenSUSE, выполненные через GUI, чтобы можно было работать с виртуальной машиной по ssh

Установка ssh сервера
```
sudo zypper refresh
sudo zypper install -y openssh-server

sudo systemctl enable sshd
sudo systemctl start sshd

# https://www.initpals.com/suse/how-to-enable-or-disable-firewall-in-sles-15/
sudo firewall-cmd --permanent --zone=public --add-port=22/tcp
sudo firewall-cmd --permanent --zone=public --add-port=11122/tcp
sudo firewall-cmd --reload
```
